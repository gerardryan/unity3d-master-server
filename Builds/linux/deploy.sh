#! /bin/bash

echo $(hostname): $(tar -vczf master-server.tar.gz master-server/ | wc -l) files compressed
scp master-server.tar.gz $1:.
ssh $1 'echo "$(hostname)": "$(tar -vxzf master-server.tar.gz | wc -l)" files extracted'
