﻿/// The MIT License (MIT)
/// Copyright(c) 2017 Gerard Ryan
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Associating with the Master server Client, Facilitates the distributes of rich game server information for presentation.
/// Author: Gerard Ryan - February 2017
/// </summary>
[Serializable] public class MasterServer : MonoBehaviour {
    public class MsgType : UnityEngine.Networking.MsgType {

        /// <summary>
        /// A message for the master server (client).
        /// </summary>
        public const short MasterServerData = 48;

        /// <summary>
        /// The highest value of the master server (client) message ids. User messages must be above this value.
        /// </summary>
        public const short MasterServerHighest = 48;

        new public static string MsgTypeToString(short value) {
            if (value <= Highest) {
                return UnityEngine.Networking.MsgType.MsgTypeToString(value);
            } else if (value == MasterServerData) {
                return "MasterServerData";
            } else {
                return string.Empty;
            }
        }
    }

    private enum DataMsgType : byte {
        /// <summary>
        /// Payload: | <see cref="MSG_OK"/> |
        /// </summary>
        MSG_OK = 0,

        /// <summary>
        /// Payload: | <see cref="MSG_ServerID"/> | <see cref="ServerID"/> serverIDs[] |
        /// </summary>
        MSG_ServerID = 1, // MSG_ServerID(s)

        /// <summary>
        /// Payload: | <see cref="MSG_NoServerIDs"/> |
        /// </summary>
        MSG_NoServerIDs = 2,

        /// <summary>
        /// Payload: | <see cref="MSG_ServerInfo"/> | <see cref="byte"/> serverInfo[<see cref="serverInfoLength"/>] | 
        /// </summary>
        MSG_ServerInfo = 3,

        /// <summary>
        /// Payload: | <see cref="CMD_SendServerID"/> |
        /// </summary>
        CMD_SendServerID = 4,

        /// <summary>
        /// Payload: | <see cref="CMD_GetServerIDs"/> |
        /// </summary>
        CMD_GetServerIDs = 5,

        /// <summary>
        /// Payload: | <see cref="CMD_RemoveServerID"/> |
        /// </summary>
        CMD_RemoveServerID = 6,

        /// <summary>
        /// Payload: | <see cref="CMD_GetInfo"/> |
        /// </summary>
        CMD_GetInfo = 7,

        /// <summary>
        /// Payload: | <see cref="CMD_NATPunchThrough"/> | <see cref="ServerID"/> dstServer |
        /// </summary>
        CMD_NATPunchThrough = 8
    }

    [Serializable] public struct ServerID {
        // +--------------------+-----------------+
        // | address (40 bytes) | port (2 bytes) |
        // +--------------------+-----------------+

        public static int AddressSize = 40;
        public static int Size = AddressSize + sizeof(UInt16);

        byte[] reccord;

        public string Address {
            get { return (reccord != null) ? System.Text.Encoding.ASCII.GetString(reccord, 0, AddressSize).Trim(new char[] { ' ', '\n', '\0', '\t' }) : null; }

            set {
                if (reccord == null || reccord.Length != Size)
                    reccord = new byte[Size];
                System.Text.Encoding.ASCII.GetBytes((value.Length < AddressSize) ? value.PadRight(AddressSize) : value.Substring(0, AddressSize)).CopyTo(reccord, 0);
            }
        }

        public UInt16 Port {
            get { return (reccord != null) ? BitConverter.ToUInt16(reccord, AddressSize) : (UInt16)0; }

            set {
                if (reccord == null || reccord.Length != Size)
                    reccord = new byte[Size];
                BitConverter.GetBytes(value).CopyTo(reccord, AddressSize);
            }
        }

        public ServerID(string address, UInt16 port) {
            reccord = new byte[Size];
            System.Text.Encoding.ASCII.GetBytes((address.Length < AddressSize) ? address.PadRight(AddressSize) : address.Substring(0, AddressSize)).CopyTo(reccord, 0);
            BitConverter.GetBytes(port).CopyTo(reccord, AddressSize);
        }

        public ServerID(byte[] serialized, int index = 0) {
            if (index == 0 && serialized.Length == Size) {
                reccord = serialized;
            } else {
                reccord = new byte[Size];
                Array.Copy(serialized, index, reccord, 0, Size);
            }
        }

        public byte[] Serialized {
            get { return reccord; }
        }

        public override string ToString() {
            return "{\"" + Address + "\", " + Port + "}";
        }

        public override int GetHashCode() {
            return (Address + Port).GetHashCode();
        }

        public override bool Equals(object obj) {
            return this.GetHashCode() == ((ServerID)obj).GetHashCode();
        }
    }
    private HashSet<ServerID> servers;
    private string serversFilename = "servers.bin";

    [SerializeField] private int serverFrameRate = 25;
    [SerializeField] private bool verboseLogging = false;

    [Header("Socket Configuration")]
    private int hostID = -1;
    [SerializeField] private string masterServerIP = null;
    [SerializeField] private UInt16 masterServerPort = 1111;
    [SerializeField] private ushort sendServerIDBuffSize = 1024;
    private int RecBuffSize = 64; // = NetBuffer initial size

    [Header("Socket Configuration")]
    [SerializeField] private GlobalConfig globalConfig;
    [SerializeField] private ConnectionConfig connectionConfig;
    [Tooltip("Should be the channel that the master server and client exclusively use to communicate.")]
    [SerializeField] private byte channelID = 2;
    [SerializeField] private int maxConnections = 1024;

    [Header("Subroutine Frequencies")]
    [SerializeField] private float serverBackup = 120f;
    [SerializeField] private float downServerCheck = 30f;
    [SerializeField] private float idleConnectionCheck = 2f;
    [SerializeField] private float idleTimeDisconnect = 4f;

    /****   Class Events   ****/
    private delegate void OnConnectEvent(int conID);
    private OnConnectEvent onConnectDelegates;
    private delegate void OnDisconnectEvent(int conID);
    private OnDisconnectEvent onDisconnectDelegates;
    private delegate void OnDataEvent(int conID, ref byte[] buffer, ref bool dataUsed);
    private OnDataEvent onDataDelegates;

    private List<float> lastTimeConnActive;
    private int connectionCount;

    /// <summary>
    /// An array of <see cref="byte[]"/> payloads for each of the <see cref="DataMsgType"/>s.
    /// </summary>
    byte[][] dataMsgCache;

    /// <summary>
    /// Initialises <see cref="dataMsgCache"/> with default messages so that they only need minor modification before sending.
    /// </summary>
    private void InitDataMsgCache() {
        byte[] dataMsgTypes = (byte[])Enum.GetValues(typeof(DataMsgType));
        dataMsgCache = new byte[dataMsgTypes.Length][];
        NetworkWriter writer = new NetworkWriter();

        foreach (byte msgType in dataMsgTypes) {
            writer.StartMessage(MsgType.MasterServerData);
            writer.Write(msgType);

            // add body if applicable 
            switch ((DataMsgType)msgType) {
                case DataMsgType.MSG_OK:
                    break;

                case DataMsgType.MSG_ServerID:
                    writer.Write(new byte[ServerID.Size], ServerID.Size); // The public ServerID in response to an added server will have to be added on the fly
                    break;

                case DataMsgType.MSG_NoServerIDs:
                case DataMsgType.MSG_ServerInfo: // not used
                case DataMsgType.CMD_SendServerID:
                case DataMsgType.CMD_GetServerIDs:
                case DataMsgType.CMD_RemoveServerID:
                case DataMsgType.CMD_GetInfo: // not used
                    break;

                case DataMsgType.CMD_NATPunchThrough:
                    writer.Write(new byte[ServerID.Size], ServerID.Size); // The ServerID will have to be added on the fly
                    break;

                default:
#if UNITY_EDITOR || DEV_BUILD
                    Debug.LogWarning("No message buffer cached for " + (DataMsgType)msgType);
#endif
                    break;
            }

            writer.FinishMessage();
            dataMsgCache[msgType] = writer.ToArray();
        }
    }

    public void Awake() {
        ParseCmdLineArgs();

        if (serverFrameRate > 0)
            Application.targetFrameRate = serverFrameRate;

        NetworkTransport.Init(globalConfig);

        if (string.IsNullOrEmpty(masterServerIP)) {
            hostID = NetworkTransport.AddHost(new HostTopology(connectionConfig, maxConnections), masterServerPort);
        } else {
            hostID = NetworkTransport.AddHost(new HostTopology(connectionConfig, maxConnections), masterServerPort, masterServerIP);
        }

        lastTimeConnActive = new List<float>();
        connectionCount = 0;
        updateBuff = new byte[RecBuffSize];

        InitDataMsgCache();

        if (File.Exists(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename)) {
            LoadServersFromFile();
        } else {
            servers = new HashSet<ServerID>();
        }

        if (idleConnectionCheck > 0)
            StartCoroutine(IdleDisconnect(idleConnectionCheck));

        if (downServerCheck > 0)
            StartCoroutine(RemoveDownServers(downServerCheck));

        if (serverBackup > 0)
            StartCoroutine(BackupServersToFile(serverBackup));
    }

    public void Stop() {
        NetworkTransport.Shutdown();
    }

    /********* Public so garbage collection doesn't work too hard unnecessarily *********/
    // unused temp
    int updateChannelID;
    UnityEngine.Networking.Types.NetworkID updateNetID;
    UnityEngine.Networking.Types.NodeID updateNodeID;

    // Connection Info
    int updateConID;
    string updateConIP;
    int updateConPort;
    int updateRecBuffSize;
    byte updateError;
    byte[] updateBuff;

    NetworkEventType updateEventType;
    NetworkReader updateBuffReader;
    ushort updateBuffSize;
    DataMsgType updateDataType;

    public void Update() {

        do { // avoid unity overhead when there is work to be done
            updateEventType = NetworkTransport.ReceiveFromHost(hostID, out updateConID, out updateChannelID, updateBuff, RecBuffSize, out updateRecBuffSize, out updateError);
            if ((NetworkError)updateError != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
                if ((NetworkError)updateError != NetworkError.Timeout)
                    Debug.LogError((NetworkError)updateError);
#endif
            }

            if (updateEventType == NetworkEventType.Nothing)
                break;

            switch (updateEventType) {
                case NetworkEventType.DataEvent:
                    if (updateChannelID == channelID) {

                        // Update connection so last active time is accurate
                        lastTimeConnActive[updateConID] = Time.realtimeSinceStartup;

                        NetworkTransport.GetConnectionInfo(hostID, updateConID, out updateConIP, out updateConPort, out updateNetID, out updateNodeID, out updateError);
                        if ((NetworkError)updateError != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
                            Debug.LogError((NetworkError)updateError);
#endif
                            break;
                        }

                        if (onDataDelegates != null) {
                            bool dataUsed = false;
                            onDataDelegates(updateConID, ref updateBuff, ref dataUsed);
                            if (dataUsed)
                                break;
                        }

                        updateBuffReader = new NetworkReader(updateBuff);
                        updateBuffSize = updateBuffReader.ReadUInt16();

                        short msgType = updateBuffReader.ReadInt16();
                        if (msgType != MsgType.MasterServerData) {
#if !(UNITY_EDITOR || DEV_BUILD)
                            if (verboseLogging)
#endif
                                Debug.LogError(System.DateTime.Now + ": Wrong MsgType received - " + MsgType.MsgTypeToString(msgType) + " (" + msgType + ")");
                            break;
                        }

                        switch (updateDataType = (DataMsgType)updateBuffReader.ReadByte()) {
                            case DataMsgType.MSG_NoServerIDs: // response to CMD_SendServerID when no server is not running
                                if (updateBuffSize == sizeof(DataMsgType)) {
                                    NetworkTransport.Disconnect(hostID, updateConID, out updateError);
#if UNITY_EDITOR || DEV_BUILD
                                    if ((NetworkError)updateError != NetworkError.Ok)
                                        Debug.LogError((NetworkError)updateError);
#endif
                                } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging)
#endif
                                        Debug.LogError(System.DateTime.Now + ": Unexpected update buffer size with MSG_NoServerIDs");
                                }
                                break;

                            case DataMsgType.CMD_GetServerIDs:
                                if (updateBuffSize == sizeof(DataMsgType)) {
                                    StartCoroutine(SendServerIDs(updateConID, (success, errorDesc) => {
#if !(UNITY_EDITOR || DEV_BUILD)
                                        if (verboseLogging) {
#endif
                                            if (!success)
                                                Debug.LogError(System.DateTime.Now + ": " + errorDesc + " {\"" + updateConIP + "\", " + updateConPort + "}");
#if !(UNITY_EDITOR || DEV_BUILD)
                                        }
#endif
                                    }));
                                } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging)
#endif
                                        Debug.LogError(System.DateTime.Now + ": Unexpected update buffer size with CMD_GetServerIDs");
                                }
                                break;

                            case DataMsgType.MSG_ServerID:
                                if (updateBuffSize == sizeof(DataMsgType)) {
                                    ServerID newServer = new ServerID(updateConIP, (UInt16)updateConPort);
                                    if (servers.Add(newServer)) {
#if !(UNITY_EDITOR || DEV_BUILD)
                                        if (verboseLogging) {
#endif
                                            Debug.Log(System.DateTime.Now + ": " + newServer + " was added");
#if !(UNITY_EDITOR || DEV_BUILD)
                                        }
#endif
                                    }

                                    // confirm server is received with their entry
                                    newServer.Serialized.CopyTo(dataMsgCache[(byte)DataMsgType.MSG_ServerID], sizeof(ushort) + sizeof(short) + sizeof(DataMsgType));
                                    NetworkTransport.Send(hostID, updateConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_ServerID], dataMsgCache[(byte)DataMsgType.MSG_ServerID].Length, out updateError);
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging) {
#endif
                                        if ((NetworkError)updateError != NetworkError.Ok)
                                            Debug.LogError(System.DateTime.Now + ": Failed to confirm with " + newServer + " that server has been added " + (NetworkError)updateError);
#if !(UNITY_EDITOR || DEV_BUILD)
                                    }
#endif
                                } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging)
#endif
                                        Debug.LogError(System.DateTime.Now + ": Unexpected update buffer size with MSG_ServerID");
                                }
                                break;

                            case DataMsgType.CMD_RemoveServerID:
                                if (updateBuffSize == sizeof(DataMsgType)) {
                                    ServerID server = new ServerID(updateConIP, (UInt16)updateConPort);

                                    if (servers.Remove(server)) {
#if !(UNITY_EDITOR || DEV_BUILD)
                                        if (verboseLogging) {
#endif
                                            Debug.Log(System.DateTime.Now + ": " + server + " was removed");
#if !(UNITY_EDITOR || DEV_BUILD)
                                        }
#endif
                                        NetworkTransport.Send(hostID, updateConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out updateError);
#if UNITY_EDITOR || DEV_BUILD
                                        if ((NetworkError)updateError != NetworkError.Ok)
                                            Debug.LogError((NetworkError)updateError);
#endif

                                    } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                        if (verboseLogging)
#endif
                                            Debug.LogError(System.DateTime.Now + ": " + server + " could not be removed");
                                        NetworkTransport.Disconnect(hostID, updateConID, out updateError);
#if UNITY_EDITOR || DEV_BUILD
                                        if ((NetworkError)updateError != NetworkError.Ok)
                                            Debug.LogError((NetworkError)updateError);
#endif
                                    }
                                } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging)
#endif
                                        Debug.LogError(System.DateTime.Now + ": Unexpected update buffer size with CMD_RemoveServerID");
                                }
                                break;

                            case DataMsgType.CMD_NATPunchThrough:
                                if (updateBuffSize == sizeof(DataMsgType) + ServerID.Size) {
                                    StartCoroutine(FacilitateNATPunchThrough(new ServerID(updateBuffReader.ReadBytes(ServerID.Size)), updateConID, new ServerID(updateConIP, (UInt16)updateConPort), (success, errorDesc) => {
#if !(UNITY_EDITOR || DEV_BUILD)
                                        if (verboseLogging) {
#endif
                                            if (!success) {
                                                Debug.LogError(System.DateTime.Now + ": " + errorDesc);
                                            } else {
                                                Debug.Log(System.DateTime.Now + ": " + errorDesc);
                                            }
#if !(UNITY_EDITOR || DEV_BUILD)
                                        }
#endif
                                    }));
                                } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                                    if (verboseLogging)
#endif
                                        Debug.LogError(System.DateTime.Now + ": Unexpected update buffer size with CMD_NATPunchThrough");
                                }
                                break;

                            default:
#if !(UNITY_EDITOR || DEV_BUILD)
                            if (verboseLogging)
#endif
                                Debug.LogWarning(System.DateTime.Now + ": Unhandled Data: " + updateDataType + " From " + updateConIP + ":" + updateConPort);
                                NetworkTransport.Disconnect(hostID, updateConID, out updateError);
#if UNITY_EDITOR || DEV_BUILD
                                if ((NetworkError)updateError != NetworkError.Ok)
                                    Debug.LogError((NetworkError)updateError);
#endif
                                break;
                        } 
                    }
                    break;

                case NetworkEventType.ConnectEvent:
                    while (lastTimeConnActive.Count <= updateConID)
                        lastTimeConnActive.Add(-1f);
                    lastTimeConnActive[updateConID] = Time.realtimeSinceStartup;
                    connectionCount++;

                    if (onConnectDelegates != null)
                        onConnectDelegates(updateConID);
                    break;

                case NetworkEventType.DisconnectEvent:
                    if (updateConID < lastTimeConnActive.Count) {
                        lastTimeConnActive[updateConID] = -1f;
                        connectionCount--;
                    }

                    if (onDisconnectDelegates != null)
                        onDisconnectDelegates(updateConID);
                    break;

                case NetworkEventType.BroadcastEvent:
                    break;
            } 
        } while (updateEventType != NetworkEventType.Nothing);
    }

    /// <summary>
    /// Sends the set of servers to the given connection.
    /// </summary>
    /// <param name="conID">The connection to send the servers to.</param>
    /// <param name="onReturn">(bool) Weather the set of servers have been sent successfully or not. (string) A description of any error that occurred.</param>
    /// <returns></returns>
    private IEnumerator SendServerIDs(int conID, Action<bool, string> onReturn) {
        ServerID[] cachedServers = new ServerID[servers.Count];
        servers.CopyTo(cachedServers);

        NetworkWriter serverIDWriter = new NetworkWriter(new byte[sendServerIDBuffSize]);
        serverIDWriter.StartMessage(MsgType.MasterServerData);
        serverIDWriter.Write((byte)DataMsgType.MSG_ServerID);

        byte error;
        int buffIDCount = 0;
        int cachedIDIndex = 0;
        int serverIDsPerBuff = (sendServerIDBuffSize - (sizeof(ushort) /* buffer length */ + sizeof(short) /* MsgType.MasterServerData */ + sizeof(DataMsgType))) / ServerID.Size;
        
        bool recOk = false;
        OnDataEvent GetOK = (int recConID, ref byte[] buffer, ref bool dataUsed) => {
            if (conID != recConID)
                return;

            NetworkReader data = new NetworkReader(buffer);
            if (data.ReadUInt16() != sizeof(DataMsgType))
                return;

            if (data.ReadInt16() != MsgType.MasterServerData)
                return;

            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_OK)
                return;

            recOk = true;
            dataUsed = true;
        };
        bool disconnected = false;
        OnDisconnectEvent TestDisconnected = (recConID) => { disconnected = (recConID == conID) ? true : disconnected; };

        onDataDelegates += GetOK;
        onDisconnectDelegates += TestDisconnected;
        while (cachedIDIndex < cachedServers.Length) {
            serverIDWriter.Write(cachedServers[cachedIDIndex].Serialized, ServerID.Size);
            buffIDCount++;
            cachedIDIndex++;

            if (buffIDCount == serverIDsPerBuff || cachedIDIndex == cachedServers.Length) { // send
                serverIDWriter.FinishMessage();
                Debug.Assert(serverIDWriter.AsArray().Length == sendServerIDBuffSize,"Malformed " + DataMsgType.MSG_ServerID + " buffer. ");
                NetworkTransport.Send(hostID, conID, channelID, serverIDWriter.AsArray(), serverIDWriter.AsArray().Length, out error);
                if ((NetworkError)error != NetworkError.Ok) {
                    onReturn(false, "Failed to send ServerID's (" + ((NetworkError)error).ToString() + ") to ");
                    onDataDelegates -= GetOK;
                    onDisconnectDelegates -= TestDisconnected;
                    yield break;
                }

                if (cachedIDIndex < cachedServers.Length) {   // reset
                    serverIDWriter.StartMessage(MsgType.MasterServerData);
                    serverIDWriter.Write((byte)DataMsgType.MSG_ServerID);
                    buffIDCount = 0;
                }

                yield return new WaitUntil(() => { return recOk || disconnected; });
                recOk = false;

                if (disconnected) {
                    onReturn(false, "Unexpected disconnection while sending ServerID's to ");
                    onDataDelegates -= GetOK;
                    onDisconnectDelegates -= TestDisconnected;
                    yield break;
                }
            }
        }
        onDataDelegates -= GetOK;
        onDisconnectDelegates -= TestDisconnected;

        NetworkTransport.Send(hostID, conID, channelID, dataMsgCache[(byte)DataMsgType.MSG_NoServerIDs], dataMsgCache[(byte)DataMsgType.MSG_NoServerIDs].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Failed to confirm that ServerID's have been sent (" + ((NetworkError)error).ToString() + ") to ");
            yield break;
        }

        onReturn(true, "");
        yield break;
    }

    /// <summary>
    /// Instructs the destination server to NAT punch through to the source server. 
    /// </summary>
    /// <param name="dstServer">The server that is requested to do NAT punch through.</param>
    /// <param name="srcServer">The server that is requesting NAT punch through.</param>
    /// <param name="onReturn">(bool) Weather NAT punch through was set up. (string) A description of any error that occurred.</param>
    /// <returns></returns>
    private IEnumerator FacilitateNATPunchThrough(ServerID dstServer, int srcConID, ServerID srcServer, Action<bool, string> onReturn) {
        bool connected = false;
        bool disconnected = false;
        bool receivedError = false;
        bool receivedOk = false;
        int dstConID = 0;
        byte error;

        OnConnectEvent TestConnected = (recConID) => { connected = (recConID == dstConID) ? true : connected; };
        OnDisconnectEvent TestDisconnected = (recConID) => { disconnected = (recConID == dstConID) ? true : disconnected; };
        OnDataEvent GetOK = (int recConID, ref byte[] buffer, ref bool dataUsed) => {
            if (recConID != dstConID)
                return;

            NetworkReader data = new NetworkReader(buffer);
            if (data.ReadUInt16() != sizeof(DataMsgType))
                return;

            if (data.ReadInt16() != MsgType.MasterServerData)
                return;

            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_OK)
                return;

            receivedOk = true;
            dataUsed = true;
        };

        // validate that requested server is one we know of
        if (!servers.Contains(dstServer)) {
            onReturn(false, "Requested NAT punch through for unknown server.  src: " + srcServer + ", dst: " + dstServer);
            yield break;
        }

        // connect to dstServer
        dstConID = NetworkTransport.Connect(hostID, dstServer.Address, dstServer.Port, 0, out error);
        if ((NetworkError)error != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
            Debug.LogError((NetworkError)error);
#endif
            onReturn(false, "Failed to connect to " + dstServer + " (" + (NetworkError)error + ')');
            yield break;
        }

        onDisconnectDelegates += TestDisconnected;
        onConnectDelegates += TestConnected;
        yield return new WaitUntil(() => { return connected || disconnected || receivedError; });
        onConnectDelegates -= TestConnected;

        if (disconnected || receivedError) {
            onDisconnectDelegates -= TestDisconnected;
            onReturn(false, "Unexpected disconnection from " + dstServer + " (" + (NetworkError)error + ')');
            yield break;
        }

        // Request and confirm NAT punch through
        srcServer.Serialized.CopyTo(dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough], sizeof(ushort) + sizeof(short) + sizeof(DataMsgType));
        NetworkTransport.Send(hostID, dstConID, channelID, dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough], dataMsgCache[(byte)DataMsgType.CMD_NATPunchThrough].Length, out error);
        if ((NetworkError)error != NetworkError.Ok) {
            onDisconnectDelegates -= TestDisconnected;
#if UNITY_EDITOR || DEV_BUILD
            Debug.LogError((NetworkError)error);
#endif
            onReturn(false, "Error while setting up NAT punch through with " + dstServer + " (" + (NetworkError)error + ')');
            yield break;
        }

        onDataDelegates += GetOK;
        yield return new WaitUntil(() => { return receivedOk || disconnected || receivedError; });
        onDataDelegates -= GetOK;
        onDisconnectDelegates -= TestDisconnected;

        if (disconnected || receivedError) {
            onReturn(false, "Unexpected disconnection from " + dstServer + " (" + (NetworkError)error + ')');
            yield break;
        }

        // inform both servers that everything is ready for NAT punch through
        NetworkTransport.Send(hostID, srcConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out error);
#if UNITY_EDITOR || DEV_BUILD
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Error while confirming NAT punch through with " + srcServer + " (" + (NetworkError)error + ')');
            yield break;
        }
#endif
        NetworkTransport.Send(hostID, dstConID, channelID, dataMsgCache[(byte)DataMsgType.MSG_OK], dataMsgCache[(byte)DataMsgType.MSG_OK].Length, out error);
#if UNITY_EDITOR || DEV_BUILD
        if ((NetworkError)error != NetworkError.Ok) {
            onReturn(false, "Error while confirming NAT punch through with " + dstServer + " (" + (NetworkError)error + ')');
            yield break;
        }
#endif

        // each end point should disconnect once they are done, IdleDisconnect() will clean up if they take too long.
        onReturn(true, "NAT punch through ok: " + srcServer + " -> " + dstServer);
    }

    /// <summary>
    /// Disconnects any idling connections
    /// </summary>
    /// <param name="repeatTimeout">if > 0 The time between checking again</param>
    /// <returns></returns>
    private IEnumerator IdleDisconnect(float repeatTimeout) {
        byte error;

        while (true) {
            if (connectionCount > 0) {
                float now = Time.realtimeSinceStartup;
                int actualConnectionCount = 0;

                for (int conID = 0; conID < lastTimeConnActive.Count; conID++) {
                    if (lastTimeConnActive[conID] < 0f) {
                        continue;
                    } else {
                        actualConnectionCount++;
                    }

                    if (now > lastTimeConnActive[conID] + idleTimeDisconnect) {
                        string conIP;
                        int conPort;
                        UnityEngine.Networking.Types.NetworkID conNetID;
                        UnityEngine.Networking.Types.NodeID conNodeID;

                        NetworkTransport.GetConnectionInfo(hostID, conID, out conIP, out conPort, out conNetID, out conNodeID, out error);
                        if ((NetworkError)error != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
                            Debug.LogError((NetworkError)error);
#endif
#if !(UNITY_EDITOR || DEV_BUILD)
                            if (verboseLogging)
#endif
                                Debug.Log(System.DateTime.Now + ": connection ID " + conID + " idling too long, Disconnecting...");
                        } else {
#if !(UNITY_EDITOR || DEV_BUILD)
                            if (verboseLogging)
#endif
                                Debug.Log(System.DateTime.Now + ": " + " {\"" + conIP + "\", " + conPort + "} idling too long, Disconnecting...");
                        }
                        NetworkTransport.Disconnect(hostID, conID, out error);
#if UNITY_EDITOR || DEV_BUILD
                        if ((NetworkError)error != NetworkError.Ok)
                            Debug.LogError((NetworkError)error);
#endif
                    }

                    // early exit check for widely varying connection count
                    if (actualConnectionCount == connectionCount)
                        break;
                }
            }
            if (repeatTimeout > 0) {
                yield return new WaitForSecondsRealtime(repeatTimeout);
            } else {
                yield break;
            }
        }
    }

    /// <summary>
    /// Attempts to connect to stored servers and removes any that are unable to be reached.
    /// </summary>
    /// <param name="repeatTimeout">The interval in seconds that this is attempted.</param>
    /// <returns></returns>
    private IEnumerator RemoveDownServers(float repeatTimeout) {
        ServerID[] cachedServer;
        byte error;

        bool connected = false;
        bool disconnected = false;
        bool responseReceived = false;
        int conID = 0;
        ServerID serverID = new ServerID();

        OnConnectEvent TestConnected = (recConID) => { connected = (recConID == conID) ? true : connected; };
        OnDisconnectEvent TestDisconnected = (recConID) => { disconnected = (recConID == conID) ? true : disconnected; };
        OnDataEvent TestResponse = (int recConID, ref byte[] buffer, ref bool dataUsed) => {
            if (recConID != conID)
                return;

            NetworkReader data = new NetworkReader(buffer);
            if (data.ReadUInt16() != sizeof(DataMsgType))
                return;

            if (data.ReadInt16() != MsgType.MasterServerData)
                return;

            if ((DataMsgType)data.ReadByte() != DataMsgType.MSG_ServerID)
                return;

            responseReceived = true;
        };

        while (true) {
            cachedServer = new ServerID[servers.Count];
            servers.CopyTo(cachedServer);

            for (int i = 0; i < cachedServer.Length; i++) {
                connected = false;
                disconnected = false;
                responseReceived = false;
                serverID = cachedServer[i];

                conID = NetworkTransport.Connect(hostID, serverID.Address, serverID.Port, 0, out error);
                if ((NetworkError)error != NetworkError.Ok) {
#if UNITY_EDITOR || DEV_BUILD
                    Debug.LogError((NetworkError)error);
#endif
                    servers.Remove(serverID);
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log(System.DateTime.Now + ": Removed " + serverID + " Error connecting (" + (NetworkError)error + ")");
                    continue;
                }

                onDisconnectDelegates += TestDisconnected;
                onConnectDelegates += TestConnected;
                yield return new WaitUntil(() => { return connected || disconnected; });
                onConnectDelegates -= TestConnected;

                if (disconnected) {
                    onDisconnectDelegates -= TestDisconnected;
                    servers.Remove(serverID);
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log(System.DateTime.Now + ": Removed " + serverID + " Couldn't Connect");
                    continue;
                }

                NetworkTransport.Send(hostID, conID, channelID, dataMsgCache[(byte)DataMsgType.CMD_SendServerID], dataMsgCache[(byte)DataMsgType.CMD_SendServerID].Length, out error);
                if ((NetworkError)error != NetworkError.Ok) {
                    onDisconnectDelegates -= TestDisconnected;
#if UNITY_EDITOR || DEV_BUILD
                    Debug.LogError((NetworkError)error); 
#endif
                    servers.Remove(serverID);
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log(System.DateTime.Now + ": Removed " + serverID + " Error communicating (" + (NetworkError)error + ")");
                    continue;
                }

                onDataDelegates += TestResponse;
                yield return new WaitUntil(() => { return responseReceived || disconnected; });
                onDataDelegates -= TestResponse;
                onDisconnectDelegates -= TestDisconnected;

                if (disconnected) {
                    servers.Remove(serverID);
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log(System.DateTime.Now + ": Removed " + serverID + " Disconnected");
                    continue;
                }

                yield return null;
            }

            yield return new WaitForSecondsRealtime(repeatTimeout);
        }
    }

    /// <summary>
    /// Saves "HashSet<ServerID> servers" to file.
    /// </summary>
    private void SaveServersToFile() {
        using (FileStream fs = File.Create(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            ServerID[] serversArray = new ServerID[servers.Count];
            servers.CopyTo(serversArray);
            binaryFormatter.Serialize(fs, serversArray);
        }
#if !(UNITY_EDITOR || DEV_BUILD)
        if (verboseLogging) 
#endif
            Debug.Log(System.DateTime.Now + ": Saved " + servers.Count + " server(s) to " + Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename);
    }

    /// <summary>
    /// Loads "HashSet<ServerID> servers" from file.
    /// </summary>
    private void LoadServersFromFile() {
        using (FileStream fs = File.Open(Application.persistentDataPath + Path.DirectorySeparatorChar + serversFilename, FileMode.Open)) {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            servers = new HashSet<ServerID>((ServerID[])binaryFormatter.Deserialize(fs));
        }
#if !(UNITY_EDITOR || DEV_BUILD)
        if (verboseLogging)
#endif
            Debug.Log(System.DateTime.Now + ": Loaded " + servers.Count + " server(s) from " + Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename);
    }

    /// <summary>
    /// Backups "HashSet<ServerID> servers" to file.
    /// </summary>
    /// <param name="repeatTimeout">The interval in seconds that this is attempted.</param>
    /// <returns></returns>
    private IEnumerator BackupServersToFile(float repeatTimeout) {
        while (true) {

            if (servers.Count > 0) {
                SaveServersToFile();
            } else if (File.Exists(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename)) {
                File.Delete(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename);
            }

            yield return new WaitForSecondsRealtime(repeatTimeout);
        }
    }

    private void OnApplicationQuit() {
        if (servers != null && servers.Count > 0) {
            SaveServersToFile();
        } else if (File.Exists(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename)) {
            File.Delete(Application.persistentDataPath + Path.AltDirectorySeparatorChar + serversFilename);
        }
    }

    /// <summary>
    /// Parses the command line arguments set at run and sets relevant variables on the server 
    /// </summary>
    private void ParseCmdLineArgs() {
        string[] args = System.Environment.GetCommandLineArgs();

        for (int i = 1; i < args.Length; i++) {
            switch (args[i].ToLower()) {
                case "-ip":
                    masterServerIP = args[++i];
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                    Debug.Log("-ip=\"" + masterServerIP + "\"");
                    break;

                case "-p":
                case "-port":
                    if (!UInt16.TryParse(args[++i], out masterServerPort)) {
                        Debug.LogErrorFormat("\nERROR! Invalid port argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-port=" + masterServerPort);
                    break;

                case "-c":
                case "-maxconnections":
                    if (!int.TryParse(args[++i], out maxConnections) || maxConnections <= 0 || maxConnections >= 65535) {
                        Debug.LogErrorFormat("\nERROR! Invalid maxConnections argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-maxconnections=" + maxConnections);
                    break;

                case "-f":
                case "-framerate":
                    if (!int.TryParse(args[++i], out serverFrameRate)) {
                        Debug.LogErrorFormat("\nERROR! Invalid frameRate argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-framerate=" + serverFrameRate);
                    break;

                case "-v":
                case "-verbose":
                    verboseLogging = true;
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("Verbose logging enabled");
                    break;

                case "-bf":
                case "-backupfreq":
                    if (!float.TryParse(args[++i], out serverBackup)) {
                        Debug.LogErrorFormat("\nERROR! Invalid backupfreq argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-backupfreq=" + serverBackup);
                    break;

                case "-dc":
                case "-downcheck":
                    if (!float.TryParse(args[++i], out downServerCheck)) {
                        Debug.LogErrorFormat("\nERROR! Invalid downcheck argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-downcheck=" + downServerCheck);
                    break;

                case "-ic":
                case "-idlecheck":
                    if (!float.TryParse(args[++i], out idleConnectionCheck)) {
                        Debug.LogErrorFormat("\nERROR! Invalid idlecheck argument: \"{0} {1}\"", args[--i], args[++i]);
                        Application.Quit();
                    }
#if !(UNITY_EDITOR || DEV_BUILD)
                    if (verboseLogging)
#endif
                        Debug.Log("-idlecheck=" + idleConnectionCheck);
                    break;

                // Ignore Unity3D args
                case "-batchmode":
                case "-nographics":
                    break;

                case "-logfile":
                    i++;
                    break;

                default:
                    Debug.LogErrorFormat("\nERROR! Unrecognised Argument: {0}", args[i]);
                    Application.Quit();
                    break;
            }
        }
    }
}
