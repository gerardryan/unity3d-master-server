### Unity3D Master Server ###

An alternative master server for use in Unity3D.

Master Server: A centralised server for accepting and distributing available game servers.

[Master Server Client][MSCRepo]

### Purpose/Motivation ###

* Avoid using Unity3D's hosted Matchmaker and relay servers.
* Allow for a simple and privately hosted service with the potential of reduced cost. 

### Configuration ###

| Field                      | Description                                                                                                         |
| :------------------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Master Server**          |                                                                                                                     |
| Server Frame Rate          | How many times per second to check for received messages.                                                           |
| Verbose Logging            | Weather or not to log a verbose amount of information to the log file.                                              |
| Master Server IP           | The IP address that the master server will bind to and listen on.                                                   |
| Master Server Port         | The port that the master server will bind to and listen on.                                                         |
| Send Server ID Buff Size   | The size of the buffer used for sending ServerID's.                                                                 |
| **Socket Configuration**   |                                                                                                                     |
| Global Config              | The Unity3D's Network configuration.                                                                                |
| Connection Config          | The Unity3D's connection/socket configuration.                                                                      |
| Channel ID                 | The ID of the channel that the master server and client exclusively use to communicate.                             |
| Max Connections            | The amount of connections that can be open simultaneously.                                                          |
| **Subroutine Frequencies** |                                                                                                                     |
| Server Backup              | The frequency in seconds that the server backups the stored ServerID's for recovery after restarting.               |
| Down Server Check          | The frequency in seconds that the server attempts to connect to all the stored servers to check if they are running |
| Idle Connection Check      | The frequency in seconds that the server checks if connections are idling (inactive).                               |
| Idle Time Disconnect       | The time in seconds a connection can be inactive for, before being classed as idling and disconnected.              |


### CLI ###

| Recognised arguments   | Argument value description                                          |
| :--------------------- | :------------------------------------------------------------------ |
| `-ip`                  | (string) Sets field Master Server IP.                               |
| `-port` `-p`           | (int) Sets field Master Server Port.                                |
| `-maxConnections` `-c` | (int) Sets field Max Connections.                                   |
| `-frameRate` `-f`      | (int) Sets field Server Frame Rate.                                 |
| `-verbose` `-v`        | (N\A) Enables verbose logging.                                      |
| `-backupfreq` `-bf`    | (float) Sets field Server Backup.                                   |
| `-downcheck` `-dc`     | (float) Sets field Down Server Check.                               |
| `-idlecheck` `-ic`     | (float) Sets field Idle Connection Check.                           |
| `-batchmode`           | (N\A) Allows program to run without the need of human intervention. |
| `-nographics`          | (N\A) When run with `-batchmode` does not initialise graphics.      |

[MSCRepo]: https://bitbucket.org/gerardryan/unity3d-master-server-client/  "Master Server Client Repository"

### License ###

All file in this project are available under the MIT License (MIT) as follows:

Copyright (c) 2017 Gerard Ryan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
